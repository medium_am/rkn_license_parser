# RKN License Parser

This project is a parser of licenses from the RKN website

## Preferences and Environment

1. Install *python3.6* or more.
1. Create environment: `python3 -m venv venv`
1. Install requirements:
```
. venv/bin/activate
pip install -U pip
pip install -r req.txt
```

## Getting started

1. Start the parser. After the parser finishes working, the json file will appear in the current directory.
```
./venb/bin/python parser.py
```
