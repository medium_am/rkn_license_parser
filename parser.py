from requests.models import Response
from bs4 import BeautifulSoup
import requests
import xmlschema
import logging
from logging.handlers import RotatingFileHandler
import re
import json


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = RotatingFileHandler("parser.log", maxBytes=100000, backupCount=5)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def get_rkn_html(url: str, headers: dict) -> str:
    try:
        response = requests.get(url, headers=headers)
        return response.content.decode(response.encoding)
    except Exception as e:
        logger.error(e)


def parse_rkn_html(html: str) -> list:
    soup = BeautifulSoup(html, "lxml")
    result = []
    data_href = None
    schema_href = None

    for tag in soup.find_all("tr"):
        children = tag.children
        for i in children:
            if "Гиперссылка (URL) на набор" in i:
                a = tag.find("a", href=True)
                try:
                    data_href = a['href']
                except KeyError as e:
                    logger.error(str(e))
            if "Описание структуры набора данных" in i:
                a = tag.find("a", href=True)
                try:
                    schema_href = a['href']
                except KeyError as e:
                    logger.error(str(e))
    
    if data_href is not None and schema_href is not None:
        result = [data_href, schema_href]
    return result


def get_data_and_schema(data_href: str, schema_href: str, site_url: str, headers: dict) -> None:
    try:
        response = requests.get(url=site_url + data_href, headers=headers)
        with open("data.xml", "w") as file:
            file.write(response.content.decode())
    except Exception as e:
        logger.error(str(e))
    try:
        response = requests.get(url=site_url + schema_href, headers=headers)
        with open("schema.xsd", "w") as file:
            file.write(response.content.decode())
    except Exception as e:
        logger.error(str(e))

 
def parse_xml(xml_file: str, schema_file: str) -> None:
    my_schema = xmlschema.XMLSchema(schema_file)
    if my_schema.is_valid(xml_file):
        try:
            with open("data.json", "w") as file:
                json.dump(my_schema.to_dict(xml_file), file, indent=2, ensure_ascii=False)
            logger.info("File was parsed into a dict")
        except FileNotFoundError as e:
            logger.error(str(e))
    else:
        logger.error("File is not valid with this schema")


if __name__ == "__main__":
    site_url = "https://rkn.gov.ru/"
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    html = get_rkn_html(url=site_url + "opendata/7705846236-LicComm/", headers=headers)
    parsed_result = parse_rkn_html(html=html)
    if len(parsed_result) > 0:
        get_data_and_schema(parsed_result[0], parsed_result[1], site_url, headers)
        parse_xml(xml_file="data.xml", schema_file="schema.xsd")
